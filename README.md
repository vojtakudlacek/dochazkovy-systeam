# Dochazkovy System

## Requirements
* Java 12
* Gradle
* Mysql database
### Employee Web
* Apache or other web server
* PHP installed
* Port forwarded port 80 
* (Optional) If you want encrypted communication you will need portforward port 443 for https and get Certificate (Free CA is Let`s encrypt)
    
## Build
    
Build app using: ```$ ./build.sh```
    
## Usage

Run app using: ```$ ./start.sh``` - Starts the GUI administration

### Web
Copy file from the ```./web/``` folder to your web server and change database setting in the dbinfo.php config file.

## Optional commandline options

+ ```$ ./start.sh --setup``` - Used to setup the application
+ ```$ ./start.sh --reader```- Starts the app in the reader mod
+ ```$ ./start.sh --showAll``` - Show`s hour's for all the IDs and all the time
+ ```$ ./start.sh --showById <ID>``` - Show's data by user ID. <ID> - User id
+ ```$ ./start.sh --calcAllHours <MMYYYY> ```  - Calculates all hour's for month for all the IDs. MMYYYY - Month int this format ex.: 022020 for Feb. 2020
+ ```$ ./start.sh --calcHoursByID <ID> <MMYYYY> ``` - Calculates hours by id and by month. ID  - User id, MMYYYY - Month int this format ex.: 022020 for Feb. 2020
+ ```$ ./start.sh --help ``` - show`s help for the application