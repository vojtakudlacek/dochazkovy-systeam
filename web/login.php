<?php
require_once 'header.html';
require_once 'dbinfo.php';
session_start();
?>
<li><a href="index.php">Domů</a></li>
</ul>
</div>
</nav>

<ul class="sidenav" id="mobile-links">
    <li><a href="index.php">Domů</a></li>
</ul>

<br>
<div class="container">
    <div class="row">
        <form method="post" class="col s12">
            <div class="row">
                <div class="input-field col s6">
                    <input id="first_name" type="text" class="validate" name="first_name">
                    <label for="first_name">Jméno</label>
                </div>
                <div class="input-field col s6">
                    <input id="last_name" type="text" class="validate" name="last_name">
                    <label for="last_name">Prijmení</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input id="password" type="password" class="validate" name="id">
                    <label for="password">ID</label>
                </div>
                <div class="input-field col s3">
                    <p>
                        <label>
                            <input type="checkbox" name="save"/>
                            <span>Zapamatovat údaje</span>
                        </label>
                    </p>
                </div>
                <div class="input-field col s3">
                    <button class="btn waves-effect waves-light" type="submit" name="send">Přihlásit se
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </div>
            <div class="row">
                <?php
                if (isset($_POST['send'])) {
                    $firstName = $_POST['first_name'];
                    $lastName = $_POST['last_name'];
                    $id = $_POST['id'];
                    $save = isset($_POST['save']);

                    $pdo = new PDO("mysql:host=" . $host . ";dbname=" . $db . ";charset=utf8mb4", $username, $password);
                    $stmt = $pdo->prepare("SELECT * FROM employee WHERE employeeEAN = ? AND name=? AND surname=?");
                    $stmt->execute(array($id, $firstName, $lastName));
                    $result = $stmt->fetchAll();
                    if (count($result) == 1) {
                        $_SESSION['ID'] = $id;
                        $_SESSION['firstName'] = $firstName;
                        $_SESSION['lastName'] = $lastName;
                        header("Location: index.php");
              } else {
                        echo "<div class='card-panel red lighten-2 white-text'>Invalid login</div>";
                    }
                } ?>
            </div>

        </form>
    </div>
</div>
<?php
require_once 'footer.html';
?>
