<?php
require 'header.html';
session_start();
error_reporting(E_ALL);
if (isset($_GET['logout'])) {
    session_destroy();
    header('Location: index.php');
}

if (!isset($_SESSION['ID'])) {
    ?>
    <li><a href="login.php">Přihlásit se</a></li>
    </ul>
    </div>
    </nav>

    <ul class="sidenav" id="mobile-links">
        <li><a href="login.php">Přihlásit se</a></li>
    </ul>
    <br>
    <div class="container color-panel teal z-depth-4">
        <br>
        <div style="text-align: center;">
            <h2><a href="login.php" class="color-panel white-text">Přihlásit se</a></h2>
        </div>
        <br>
    </div>
    <?php
} else {
    ?>
    <li><a><?php echo $_SESSION['firstName'] . " " . $_SESSION['lastName']; ?></a></li>
    <li><a href="?logout=1">Odhlásit se</a></li>
    </ul>
    </div>
    </nav>

    <ul class="sidenav" id="mobile-links">
        <li><a><?php echo $_SESSION['firstName'] . " " . $_SESSION['lastName']; ?></a></li>
        <li><a href="?logout">Odhlásit se</a></li>
    </ul>

    <div class="container">
        <table class="striped centered">
            <thead>
            <tr>
                <th>Datum</th>
                <th>Příchod</th>
                <th>Odchod</th>
            </tr>
            </thead>

            <tbody>
            <?php
            require_once 'dbinfo.php';
            $pdo = new PDO("mysql:host=" . $host . ";dbname=" . $db . ";charset=utf8mb4", $username, $password);
            $arrivals = $pdo->query("SELECT * FROM arrival where employee_employeeEAN=" . $_SESSION['ID'] . ";")->fetchAll();
            $deparatures = $pdo->query("SELECT * FROM departure where employee_employeeEAN=" . $_SESSION['ID'] . ";")->fetchAll();

            for ($i = 0; $i < sizeof($deparatures); $i++) {
                $a = new DateTime($arrivals[$i]['time']);
                $d = new DateTime($deparatures[$i]['time']);

                echo "<tr><td>" . $a->format("d.m.Y") . "</td><td>" . $a->format("H:m:s.v") . "</td><td>". $d ->format("H:m:s.v")."</td></tr>";
            }
            ?>
            </tbody>
        </table>
    </div>
    <?php
}
require_once 'footer.html';
?>
