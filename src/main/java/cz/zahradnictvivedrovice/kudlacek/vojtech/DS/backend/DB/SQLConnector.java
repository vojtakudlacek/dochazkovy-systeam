package cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.DB;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface SQLConnector {
    boolean execute(String sql) throws SQLException;

    ResultSet query(String sql) throws SQLException;

    ResultSet select(String colonNames, String tableName, String condition) throws SQLException;

    boolean insert(String colonNames, String values, String table) throws SQLException;

    void close() throws SQLException;
}
