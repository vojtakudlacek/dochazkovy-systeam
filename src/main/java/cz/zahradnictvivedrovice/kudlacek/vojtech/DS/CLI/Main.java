package cz.zahradnictvivedrovice.kudlacek.vojtech.DS.CLI;

import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.Configuration;
import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.DB.MysqlDatabaseConnector;
import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.DB.SQLConnector;
import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.JMLSP;
import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.frontend.MainWindow;
import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.frontend.SettingsWindow;
import mdlaf.MaterialLookAndFeel;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;

public class Main {
    public static CLI cli;
    public static Configuration config;
    public static JMLSP jmlsp;

    /**
     * Main function
     *
     * @param args - CLI arguments
     */
    public static void main(String[] args) {
        //Setting up Configuration
        config = new Configuration(new File(System.getProperty("user.home") + System
                .getProperty("file.separator") + ".DS"), "DBConfig");

        //Setting up JMLSP
        try {
            jmlsp = new JMLSP(System.getProperty("user.home") + System.getProperty("file.separator") +
                    ".DS" + System.getProperty("file.separator") + "languages", "en_us.lang");
        } catch (IOException e) {
            System.out.println("Unable to load language");
        }

        //Language selection and configuration setup
        if (!config.isNameSet("lang")) {
            String lang;
            if ((args.length == 0 || args[0].equals(""))) {
                try {
                    SettingsWindow settingsWindow = new SettingsWindow(false);
                    //waiting until the window is not closed.
                    while (settingsWindow.isDisplayable()) ;
                } catch (Exception e) {
                    //If there is not GUI
                    CLI.setUp(config);
                }
            } else {
                //If there is GUI, but we don't want it to use it
                CLI.setUp(config);
            }
        } else {
            //If language is set in config
            jmlsp.setLanguage(config.getConfigByName("lang"));
        }

        //If there is setup argument
        if (args.length >= 1 && args[0].equals("--setup"))
            CLI.setUp(config);
        //If there is not DBHost set
        if (!config.isNameSet("DBHost")) {
            System.err.println(jmlsp.getByID("DBNotSetError"));
            System.exit(1);
        }
        SQLConnector mysqlDatabaseConnector = null;

        //DB Creation
        try {
            mysqlDatabaseConnector = new MysqlDatabaseConnector(new URI(config.getConfigByName("DBHost")),
                    config.getConfigByName("DBUser"), config.getConfigByName("DBPass"), config.getConfigByName("DBDB"));
        } catch (SQLException | URISyntaxException e) {
            System.err.println(jmlsp.getByID("DBConnErr"));
            System.exit(1);
        } catch (ClassNotFoundException e) {
            System.err.println(jmlsp.getByID("DBDriverErr"));
            System.exit(1);
        }

        //Creating cli object
        cli = new CLI(mysqlDatabaseConnector);

        //If there is arguments run processArgs() otherwise run gui
        if (args.length > 0) {
                processArgs(args);
        } else {
            try {
                try {
                    UIManager.setLookAndFeel(new MaterialLookAndFeel());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new MainWindow(1280, 720);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Processing CLI arguments
     *
     * @param args - CLI arguments
     */
    private static void processArgs(String[] args) {
        switch (args[0]) {
            case "--reader":
                cli.startReading();
                break;
            case "--showAll":
                cli.showAll();
                break;
            case "--showById":
                if (args.length != 2) {
                    System.err.println(jmlsp.getByID("BadArgs"));
                    System.exit(1);
                } else {
                    cli.showByID(args[1]);
                }
                break;
            case "--calcAllHours":
                try {
                    if (args.length != 2) {
                        System.err.println(jmlsp.getByID("BadArgs"));
                        System.exit(1);
                    } else {
                        cli.calculateAllHours(args[1]);
                    }
                } catch (IOException e) {
                    System.err.println(jmlsp.getByID("PubHol"));
                }
                break;
            case "--calcHoursByID":
                try {
                    if (args.length != 3) {
                        System.err.println(jmlsp.getByID("BadArgs"));
                        System.exit(1);
                    } else {
                        cli.calculateHours(args[2], args[1]);
                    }
                } catch (IOException e) {
                    System.err.println(jmlsp.getByID("PubHol"));
                }
                break;
            case "--help":
                cli.showHelp();
                break;
            default:
                System.out.println(jmlsp.getByID("Hhelp"));
        }
    }
}
