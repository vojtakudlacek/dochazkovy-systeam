package cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend;

import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.CLI.Main;
import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.DB.MysqlDatabaseConnector;
import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.DB.SQLConnector;
import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.data.AddById;
import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.data.DataGetter;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class SaveThread implements Runnable{

    private AddById addById;
    private DataGetter dataGetter;
    private String data;

    public SaveThread(SQLConnector mysqlDatabaseConnector, String data){
        this.addById = new AddById(mysqlDatabaseConnector);
        this.dataGetter = new DataGetter(mysqlDatabaseConnector);
        this.data = data;
    }

    @Override
    public void run() {
        try {
            ArrayList<String[]> compareData = this.dataGetter.getDataById(data);
            String[] last = compareData.get(compareData.size() - 1);
            if (last[last.length - 2] == null) {
                addById.add(data, null);
                System.out.println("\007");
                return;
            }
            if (last[last.length - 1] == null || last[last.length -1].equals("NONE")) {
                if (LocalDateTime.now().atZone(ZoneId.systemDefault()).toEpochSecond() -
                        LocalDateTime.parse(last[last.length - 2], DateTimeFormatter.ISO_DATE_TIME)
                                .atZone(ZoneId.systemDefault()).toEpochSecond() > 60) {

                    addById.add(data, null);
                    System.out.println("\007");
                    return;
                }
                return;
            }
            if (LocalDateTime.now().atZone(ZoneId.systemDefault()).toEpochSecond() -
                    LocalDateTime.parse(last[last.length - 1], DateTimeFormatter.ISO_DATE_TIME)
                            .atZone(ZoneId.systemDefault()).toEpochSecond() > 60) {

                addById.add(data, null);
                System.out.println("\007");
            }
        } catch (SQLIntegrityConstraintViolationException e) {
            System.out.println(Main.jmlsp.getByID("BadChip") + " \007 \007");
        } catch (SQLException sqlException) {
            System.err.println("Error while connecting to database !");
            System.exit(1);
        }
    }
}
