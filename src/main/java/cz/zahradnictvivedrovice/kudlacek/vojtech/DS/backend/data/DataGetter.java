package cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.data;

import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.DB.SQLConnector;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DataGetter {

    private final SQLConnector mysqlDatabaseConnector;

    /**
     * Class that getting data from database
     *
     * @param mysqlDatabaseConnector - Created mysqlDatabaseConnector
     */
    public DataGetter(SQLConnector mysqlDatabaseConnector) {
        this.mysqlDatabaseConnector = mysqlDatabaseConnector;
    }

    /**
     * Getting all data about all ids from database
     *
     * @return ArrayList<String [ ]>
     * @throws SQLException
     */
    public ArrayList<String[]> getAllData() throws SQLException {

        ResultSet resultSeta = this.mysqlDatabaseConnector
                .query("SELECT employeeEAN, name, surname, arrival.time as a FROM employee \n" +
                        "LEFT JOIN arrival ON employee.employeeEAN=arrival.employee_employeeEAN ORDER BY employeeEAN,a");
        ResultSet resultSetd = this.mysqlDatabaseConnector
                .query("SELECT employeeEAN, name, surname, departure.time as d FROM employee " +
                        "LEFT JOIN departure ON employee.employeeEAN=departure.employee_employeeEAN ORDER BY employeeEAN,d");

        return processResultSets(resultSeta, resultSetd);
    }

    /**
     * Processing resultSets to ArrayList<String[]>
     *
     * @param resultSeta - resultSet of arrivals
     * @param resultSetd - resultSet of departs
     * @return ArrayList<String [ ]>
     * @throws SQLException
     */
    private ArrayList<String[]> processResultSets(ResultSet resultSeta, ResultSet resultSetd) throws SQLException {
        ArrayList<String[]> data = new ArrayList<>();

        while (resultSeta.next()) {
            String[] row = new String[5];
            row[0] = String.valueOf(resultSeta.getInt("employeeEAN"));
            row[1] = resultSeta.getString("name");
            row[2] = resultSeta.getString("surname");
            row[3] = resultSeta.getString("a");

            if (resultSetd.next() && row[0].equals(String.valueOf(resultSetd.getInt("employeeEAN")))) {
                row[4] = resultSetd.getString("d");
            } else {
                row[4] = "NONE";
            }
            data.add(row);
        }

        return data;
    }

    /**
     * Getting all data about @param id from database
     *
     * @param id - String id of the user
     * @return ArrayList<String [ ]> of data
     * @throws SQLException
     */
    public ArrayList<String[]> getDataById(String id) throws SQLException {
        ResultSet resultSeta = this.mysqlDatabaseConnector
                .query("SELECT employeeEAN, name, surname, arrival.time as a FROM employee \n" +
                        "LEFT JOIN arrival ON employee.employeeEAN=arrival.employee_employeeEAN where employeeEAN="
                        + id + " ORDER BY employeeEAN,a ");

        ResultSet resultSetd = this.mysqlDatabaseConnector
                .query("SELECT employeeEAN, name, surname, departure.time as d FROM employee " +
                        "LEFT JOIN departure ON employee.employeeEAN=departure.employee_employeeEAN WHERE employeeEAN="
                        + id + " ORDER BY employeeEAN,d");

        return processResultSets(resultSeta, resultSetd);
    }
}
