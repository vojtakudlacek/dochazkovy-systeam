package cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.DB;

import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.CLI.Main;

import java.net.URI;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MysqlDatabaseConnector implements SQLConnector {

    private Connection connection;

    private URI url;

    private String username;
    private String password;
    private String database;

    /**
     * Database connector for mysql
     *
     * @param url      - IP or URL of mysql server
     * @param username - username for mysql
     * @param password - password for mysql
     * @param database - database from mysql
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public MysqlDatabaseConnector(URI url, String username, String password, String database) throws
            ClassNotFoundException, SQLException {
        this.url = url;
        this.username = username;
        this.password = password;
        this.database = database;
        Class.forName("com.mysql.cj.jdbc.Driver");
        connection = DriverManager.getConnection(String
                .format("jdbc:mysql://%s/%s?user=%s&password=%s&useUnicode=true&serverTimezone=UTC",
                        this.url.getRawPath(),
                        this.database,
                        this.username,
                        this.password));

        if (!this.query("SHOW TABLES LIKE 'arrival'").first())
            this.createTables();
    }

    /**
     * Creating tables in new database
     *
     * @throws SQLException
     */
    private void createTables() throws SQLException {
        ArrayList<String> createScripts = new ArrayList<>();

        createScripts.add("CREATE TABLE `arrival` (`IDArrival` int(11) NOT NULL, `time` varchar(45) NOT NULL," +
                " `employee_employeeEAN` int(11) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");
        createScripts.add("CREATE TABLE `departure` (`IDDeparture` int(11) NOT NULL, `time` varchar(45) NOT NULL," +
                "`employee_employeeEAN` int(11) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");
        createScripts.add("CREATE TABLE `employee` (`employeeEAN` int(11) NOT NULL, `name` varchar(45) NOT NULL, " +
                "`surname` varchar(45) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");
        createScripts.add("ALTER TABLE `arrival` ADD PRIMARY KEY (`IDArrival`)," +
                " ADD KEY `employee_employeeEAN` (`employee_employeeEAN`);");
        createScripts.add("ALTER TABLE `departure` ADD PRIMARY KEY (`IDDeparture`), " +
                "ADD KEY `employee_employeeEANs` (`employee_employeeEAN`);");
        createScripts.add("ALTER TABLE `employee`  ADD PRIMARY KEY (`employeeEAN`);");
        createScripts.add("ALTER TABLE `arrival` MODIFY `IDArrival` int(11) NOT NULL AUTO_INCREMENT;");
        createScripts.add("ALTER TABLE `departure` MODIFY `IDDeparture` int(11) NOT NULL AUTO_INCREMENT;");
        createScripts.add("ALTER TABLE `arrival` ADD CONSTRAINT `employee_employeeEAN` " +
                "FOREIGN KEY (`employee_employeeEAN`) REFERENCES `employee` (`employeeEAN`);");
        createScripts.add("ALTER TABLE `departure` ADD CONSTRAINT `employee_employeeEANs`" +
                " FOREIGN KEY (`employee_employeeEAN`) REFERENCES `employee` (`employeeEAN`);");

        for (String createScript : createScripts) {
            this.connection.createStatement().executeUpdate(createScript);
        }

        System.out.println(Main.jmlsp.getByID("DBCreated"));
    }

    /**
     * Executing non return sql command
     *
     * @param sql - sql command
     * @return success ?
     * @throws SQLException
     */
    public boolean execute(String sql) throws SQLException {
        recreateConnection();
        return this.connection.createStatement().execute(sql);
    }

    /**
     * Executing only return sql commands
     *
     * @param sql - sql command
     * @return ResultSet of data
     * @throws SQLException
     */
    public ResultSet query(String sql) throws SQLException {
        recreateConnection();
        return this.connection.createStatement().executeQuery(sql);
    }

    private void recreateConnection() throws SQLException {
        //this.close();
        connection = DriverManager.getConnection(String
                .format("jdbc:mysql://%s/%s?user=%s&password=%s&characterEncoding=UTF-8&serverTimezone=UTC",
                        this.url.getRawPath(),
                        this.database,
                        this.username,
                        this.password));
    }

    /**
     * Select command function
     *
     * @param colonNames - names of columns
     * @param tableName  - name of the table
     * @param condition  - where condition
     * @return ResultSet Of data
     * @throws SQLException
     */
    public ResultSet select(String colonNames, String tableName, String condition) throws SQLException {
        return this.query(String.format("SELECT %s from %s where %s", colonNames, tableName, condition));
    }

    /**
     * Inserting of data to database
     *
     * @param colonNames - colons names for inserting
     * @param values     - values of of inserting
     * @param table      - table inserting to
     * @return success ?
     * @throws SQLException
     */
    public boolean insert(String colonNames, String values, String table) throws SQLException {
        return this.execute(String.format("INSERT INTO %s (%s) VALUES (%s)", table, colonNames, values));
    }

    /**
     * Closing the database
     *
     * @throws SQLException
     */
    public void close() throws SQLException {
        connection.close();
    }

    @Override
    protected void finalize() throws Throwable {
        if (!connection.isClosed()) {
            this.close();
        }
        super.finalize();
    }
}
