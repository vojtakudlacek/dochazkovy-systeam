package cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.DB;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ArrivalParser {

    private final SQLConnector dbConnector;

    /**
     * Parsing arrivals from and to the database
     *
     * @param connector - Connected sql database connector
     */
    public ArrivalParser(SQLConnector connector) {
        this.dbConnector = connector;
    }

    /**
     * Adding arrival to database
     *
     * @param id       - id of the user
     * @param dateTime - time of arial or Null for now
     * @return success ?
     * @throws SQLException
     */
    public boolean addArrival(String id, LocalDateTime dateTime) throws SQLException {
        String dateTimes;

        if (dateTime == null)
            dateTimes = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME);
        else
            dateTimes = dateTime.format(DateTimeFormatter.ISO_DATE_TIME);

        return dbConnector.insert("time, employee_employeeEAN", "'" + dateTimes + "'"
                + ", " + id, "arrival");
    }

    /**
     * Getting count of arrivals from database
     *
     * @param id - String id of user
     * @return int - count
     * @throws SQLException
     */
    public int getCountOfArrivals(String id) throws SQLException {
        ResultSet rs = dbConnector.select("count(IDArrival) as c", "arrival",
                "employee_employeeEAN=" + id);
        rs.next();

        return rs.getInt("c");
    }
}
