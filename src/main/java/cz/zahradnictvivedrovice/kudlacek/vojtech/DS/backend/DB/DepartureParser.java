package cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.DB;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DepartureParser {

    private final SQLConnector connector;

    /**
     * Class DepartureParser
     *
     * @param connector - Connected connector
     */
    public DepartureParser(SQLConnector connector) {
        this.connector = connector;
    }

    /**
     * Adding depart by id
     *
     * @param id        - id of the user
     * @param dateTimei - dateTime of depart or Null for now
     * @return success ?
     * @throws SQLException
     */
    public boolean addDepart(String id, LocalDateTime dateTimei) throws SQLException {
        String dateTime;

        if (dateTimei == null)
            dateTime = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME);
        else
            dateTime = dateTimei.format(DateTimeFormatter.ISO_DATE_TIME);

        return connector.insert("time, employee_employeeEAN", "'" + dateTime + "'" + ", " +
                id, "departure");
    }

    /**
     * Getting count of departures by @param id
     *
     * @param id - String id of the employee
     * @return int - count of Departures
     * @throws SQLException
     */
    public int getCountOfDepartures(String id) throws SQLException {
        ResultSet rs = connector.select("count(IDDeparture) as c", "departure",
                "employee_employeeEAN=" + id);
        rs.next();

        return rs.getInt("c");
    }
}
