package cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.data;

import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.DB.ArrivalParser;
import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.DB.DepartureParser;
import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.DB.SQLConnector;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class AddById {
    private final ArrivalParser arrivalParser;
    private final DepartureParser departureParser;
    private final DataGetter dataGetter;

    /**
     * Object that adding arrival or departed by id
     *
     * @param dbConnector
     */
    public AddById(@NonNull SQLConnector dbConnector) {
        this.arrivalParser = new ArrivalParser(dbConnector);
        this.departureParser = new DepartureParser(dbConnector);
        this.dataGetter = new DataGetter(dbConnector);
    }

    /**
     * Method that adding arrival or departed by id
     *
     * @param id       - String id of the user
     * @param dateTime - Null or datetime to complete last uncompleted
     * @return success?
     * @throws SQLException
     */
    public boolean add(String id, LocalDateTime dateTime) throws SQLException {
        boolean success;
        if (dateTime == null)
            dateTime = LocalDateTime.now();

        ArrayList<String[]> data = this.dataGetter.getDataById(id);
        LocalDateTime arrivalDate = null;

        for (String[] dat : data) {
            if (!(dat[3] == null)) {
                if (LocalDateTime.parse(dat[3]).format(DateTimeFormatter.ofPattern("ddMMyyyy"))
                        .equals(dateTime.format(DateTimeFormatter.ofPattern("ddMMyyyy"))) && dat[4] == null) {
                    arrivalDate = LocalDateTime.parse(dat[3]);
                    break;
                }
            }
        }
        if (arrivalDate == null)
            arrivalDate = dateTime;

        if (!(this.arrivalParser.getCountOfArrivals(id) == this.departureParser.getCountOfDepartures(id)) &&
                arrivalDate.format(DateTimeFormatter.ofPattern("ddMMyyyy"))
                        .equals(dateTime.format(DateTimeFormatter.ofPattern("ddMMyyyy"))))
            success = this.departureParser.addDepart(id, dateTime);
        else
            success = this.arrivalParser.addArrival(id, dateTime);

        return !success;
    }

}
