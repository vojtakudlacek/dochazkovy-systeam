package cz.zahradnictvivedrovice.kudlacek.vojtech.DS.frontend;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.Objects;

public class AboutWindow extends JFrame {

    public AboutWindow() throws IOException {
        super("About");
        this.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {
                if (keyEvent.getKeyChar() == KeyEvent.VK_ESCAPE) {
                    setVisible(false);
                }
            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
            }
        });
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setPreferredSize(new Dimension(640, 250));
        setUndecorated(true);
        setLocationRelativeTo(null);
        this.setResizable(false);
        initComponents();
        ClassLoader classLoader = getClass().getClassLoader();
        this.setIconImage(ImageIO.read((Objects.requireNonNull(classLoader.getResource("logo.png")))));
        setLayout(new GridLayout(0, 1, 10, 0));
        pack();
        setVisible(true);
    }

    private void initComponents() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        BufferedImage wPic = ImageIO.read((Objects.requireNonNull(classLoader.getResource("logo.png"))));
        JLabel wIcon = new JLabel(new ImageIcon(wPic));
        wIcon.setAlignmentX(Component.CENTER_ALIGNMENT);
        JLabel text = new JLabel("Docházkový systém", SwingConstants.CENTER);
        text.setFont(new Font("Sans", Font.BOLD, 24));
        JLabel licence = new JLabel("<HTML><u>This software is under GNU GENERAL PUBLIC LICENSE</u></HTML>", SwingConstants.CENTER);
        licence.setFont(new Font("Sans", Font.PLAIN, 14));
        JLabel author = new JLabel("Author: Vojtěch Kudláček", SwingConstants.CENTER);
        author.setFont(new Font("Sans", Font.PLAIN, 14));
        JLabel copyright = new JLabel("Copiright: 2019 - " + LocalDateTime.now().getYear() + " © ", SwingConstants.CENTER);
        copyright.setFont(new Font("Sans", Font.PLAIN, 14));
        licence.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                try {
                    Desktop.getDesktop().browse(URI.create("https://www.gnu.org/licenses/gpl-3.0.en.html"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {
            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {
            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {
            }
        });
        text.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(wIcon);
        this.add(text);
        this.add(licence);
        this.add(author);
        this.add(copyright);
    }
}
