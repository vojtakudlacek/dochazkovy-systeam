package cz.zahradnictvivedrovice.kudlacek.vojtech.DS.frontend;

import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.CLI.Main;
import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.data.AddById;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Objects;

public class MainWindow extends JFrame {

    JTable infoTable;
    DefaultTableModel tm;
    /**
     * Main window of gui
     *
     * @param width - width of the window
     * @param height - height of the window
     * @throws SQLException - error
     */
    public MainWindow(int width, int height) throws SQLException {
        super("DS");
        setPreferredSize(new Dimension(width, height));
        ClassLoader classLoader = getClass().getClassLoader();
        try {
            this.setIconImage(ImageIO.read((Objects.requireNonNull(classLoader.getResource("logo.png")))));
        } catch (IOException e) {
            e.printStackTrace();
        }
        addMenuBar();
        addComponents();
        pack();
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    /**
     * Adding top menu to Main window
     */
    private void addMenuBar() {
        JMenuBar bar = new JMenuBar();

        JMenu file = new JMenu(Main.jmlsp.getByID("File"));
        JMenuItem settings = new JMenuItem(Main.jmlsp.getByID("Setts"));
        settings.addActionListener(actionEvent -> new SettingsWindow());
        file.add(settings);

        JMenuItem exit = new JMenuItem(Main.jmlsp.getByID("Exit"));
        exit.addActionListener(actionEvent -> System.exit(0));
        file.add(exit);

        bar.add(file);

        JMenu calculation = new JMenu(Main.jmlsp.getByID("Calc"));
        JMenuItem cpmpu = new JMenuItem(Main.jmlsp.getByID("CalcIdPM"));
        cpmpu.addActionListener(actionEvent -> {
            try {
                showCalculation(true, true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        calculation.add(cpmpu);

        JMenuItem cpm = new JMenuItem(Main.jmlsp.getByID("CalcAllPM"));
        cpm.addActionListener(actionEvent -> {
            try {
                showCalculation(true, false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        calculation.add(cpm);

        JMenuItem call = new JMenuItem(Main.jmlsp.getByID("CalcAll"));
        call.addActionListener(actionEvent -> {
            try {
                showCalculation(false, false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        calculation.add(call);

        bar.add(calculation);

        JMenu manualAddition = new JMenu(Main.jmlsp.getByID("MAdd"));

        JMenuItem addById = new JMenuItem(Main.jmlsp.getByID("MAbyId"));
        addById.addActionListener(actionEvent -> {
            try {
                addById();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
        manualAddition.add(addById);

        JMenuItem addNewUser = new JMenuItem(Main.jmlsp.getByID("ANUser"));
        addNewUser.addActionListener(actionEvent -> {
            try {
                addNewUser();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
        manualAddition.add(addNewUser);

        bar.add(manualAddition);

        JMenu help = new JMenu(Main.jmlsp.getByID("Help"));

        JMenuItem helpI = new JMenuItem(Main.jmlsp.getByID("SHelp"));
        helpI.addActionListener(actionEvent -> showHelp());
        help.add(helpI);

        JMenuItem about = new JMenuItem(Main.jmlsp.getByID("About"));
        about.addActionListener(actionEvent -> {
            try {
                new AboutWindow();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        help.add(about);
        bar.add(help);
        this.add(bar, BorderLayout.NORTH);
    }

    /**
     * Adding new user
     *
     * @throws SQLException
     */
    private void addNewUser() throws SQLException {
        String id = JOptionPane.showInputDialog(this, Main.jmlsp.getByID("ECard"), Main.jmlsp.getByID("ANUser"));
        String name = JOptionPane.showInputDialog(this, Main.jmlsp.getByID("EName"));
        String surname = JOptionPane.showInputDialog(this, Main.jmlsp.getByID("ESurname"));
        Main.cli.addNewUser(id, name, surname);
        JOptionPane.showMessageDialog(this, Main.jmlsp.getByID("Suc"));
        updateTable();

    }

    /**
     * Openig help dioalog
     */
    private void showHelp() {
        new HelpDialog();
    }

    /**
     * Adding arial or depart for user id
     */
    private void addById() throws SQLException {
        final String id = JOptionPane.showInputDialog(this, Main.jmlsp.getByID("EId"));
        DateTimePickerDialog dateTimePickerDialog = new DateTimePickerDialog();
        dateTimePickerDialog.addActionListener(actionEvent -> {
            LocalDateTime dateTime = dateTimePickerDialog.getDate();
            dateTimePickerDialog.dispose();
            ArrayList<String[]> data = null;
            try {
                data = Main.cli.dataGetter.getDataById(id);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            assert data != null;
            AddById addById = new AddById(Main.cli.mysqlDatabaseConnector);
            try {
                addById.add(id, dateTime);
                updateTable();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        });
    }

    /**
     * Calculating hours and showing dialog with results
     *
     * @param month
     * @param user
     * @throws IOException
     */
    private void showCalculation(boolean month, boolean user) throws IOException {
        String[] months = {"01", "02", "03", "04", "05", "06", "07", "09", "10", "11", "12"}; //TODO: Beautify this
        if (month && user) {
            String m = (String) JOptionPane.showInputDialog(this, Main.jmlsp.getByID("EMonth"),
                    Main.jmlsp.getByID("EMonth"), JOptionPane.QUESTION_MESSAGE, null, months, months[0]);
            int id = Integer.parseInt(JOptionPane.showInputDialog(this,
                    Main.jmlsp.getByID("EIdOfUser")));
            JOptionPane.showMessageDialog(this, Main.cli.calculateHours(m + LocalDateTime.now().getYear(), String.valueOf(id)));
        } else if (month) {
            String m = (String) JOptionPane.showInputDialog(this, Main.jmlsp.getByID("EMonth"),
                    Main.jmlsp.getByID("EMonth"), JOptionPane.QUESTION_MESSAGE, null, months, months[0]);
            JOptionPane.showMessageDialog(this, Main.cli.calculateHours(m + LocalDateTime.now().getYear(), null));
            System.out.print(m);
        } else {
            JOptionPane.showMessageDialog(this, Main.cli.calculateAllHours(null));
        }
    }

    /**
     * Adding componets to main window
     *
     * @throws SQLException
     */
    private void addComponents() throws SQLException {
        this.addDBToWidow();
    }

    /**
     * Adding table to main window
     *
     * @throws SQLException
     */
    private void addDBToWidow() throws SQLException {

        ArrayList<String[]> data = Main.cli.dataGetter.getAllData();
        String[][] dat = new String[data.size()][];
        for (int i = 0; i < data.size(); i++) {
            String[] row = data.get(i);
            dat[i] = row;
        }

        String[] columns = {"ID", "Name", "Surname", "Arrival", "Departed"};

        infoTable = new JTable(dat, columns);;
        tm = new DefaultTableModel();
        tm.setColumnIdentifiers(columns);
        tm.setDataVector(dat,columns);
        infoTable.setModel(tm);
        this.add(new JScrollPane(infoTable));
    }

    private void updateTable() throws SQLException {

        ArrayList<String[]> data = Main.cli.dataGetter.getAllData();

        tm.setRowCount(data.size());
        for (int i = 0; i < data.size(); i++) {
            for (int j = 0; j < data.get(i).length; j++) {
                infoTable.getModel().setValueAt(data.get(i)[j], i, j);
            }
        }
    }
}
