package cz.zahradnictvivedrovice.kudlacek.vojtech.DS.CLI;

import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.Configuration;
import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.DB.SQLConnector;
import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.SaveThread;
import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.data.DataGetter;
import org.checkerframework.checker.nullness.qual.NonNull;

import javax.swing.*;
import java.io.*;
import java.net.URL;
import java.sql.SQLException;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class CLI {

    public SQLConnector mysqlDatabaseConnector;
    public DataGetter dataGetter;

    /**
     * Commandline interface object
     *
     * @param mysqlDatabaseConnector
     */
    CLI(@NonNull SQLConnector mysqlDatabaseConnector) {
        this.mysqlDatabaseConnector = mysqlDatabaseConnector;
        this.dataGetter = new DataGetter(mysqlDatabaseConnector);
    }

    /**
     * Running card reader
     */
    void startReading() {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            String data = scanner.next();
            if (data.equals("exit"))
                break;
            SaveThread saveThread = new SaveThread(this.mysqlDatabaseConnector, data);
            Thread thread = new Thread(saveThread);
            thread.start();
        }
    }

    /**
     * initial setup
     *
     * @param config
     */
    public static void setUp(@NonNull Configuration config) {
        Scanner scn = new Scanner(System.in);

        int last = 0;
        for (int i = 0; i < Main.jmlsp.getLanguages().size(); i++) {
            System.out.println(i + ". - " + Main.jmlsp.getLanguages().toArray()[i]);
            last = i;
        }
        System.out.print("Select(0 - " + last + "): ");
        String lang = (String) Main.jmlsp.getLanguages().toArray()[scn.nextInt()];
        Main.jmlsp.setLanguage(lang);
        scn.nextLine();

        config.setConfig("lang", lang);

        Console csn = System.console();
        System.out.print(Main.jmlsp.getByID("InputState"));
        config.setConfig("State", scn.nextLine());
        System.out.print(Main.jmlsp.getByID("InputDBHost"));
        config.setConfig("DBHost", scn.nextLine());
        System.out.print(Main.jmlsp.getByID("InputDBUser"));
        config.setConfig("DBUser", scn.nextLine());
        System.out.print(Main.jmlsp.getByID("InputDBPass"));
        config.setConfig("DBPass", String.valueOf(csn.readPassword()));
        System.out.print(Main.jmlsp.getByID("InputDBName"));
        config.setConfig("DBDB", scn.nextLine());


        System.exit(0);
    }

    /**
     * Shows all data
     */
    void showAll() {
        ArrayList<String[]> allData = null;
        try {
            allData = this.dataGetter.getAllData();
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
        showData(allData);

    }

    /**
     * Showing data by ID
     *
     * @param id - id of employee
     */
    void showByID(String id) {
        ArrayList<String[]> idData = null;
        try {
            idData = this.dataGetter.getDataById(id);
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
        showData(idData);
    }

    /**
     * Printing data to console
     *
     * @param idData - list of data of one or more ids
     */
    private void showData(@NonNull ArrayList<String[]> idData) {
        System.out.println(String.format("%s\t%s\t%s\t%s\t\t\t\t\t\t%s", Main.jmlsp.getByID("code"), Main.jmlsp.getByID("Name"), Main.jmlsp.getByID("Surname"), Main.jmlsp.getByID("Arrival"),
                Main.jmlsp.getByID("Depart")));
        for (String[] row : idData)
            System.out.println(String.format("%s\t%s\t%s\t%s\t%s", row[0], row[1], row[2], row[3], row[4]));
    }

    /**
     * Showing help for CLI
     */
    void showHelp() {
        System.out.println(Main.jmlsp.getByID("TIHFTDS"));
        System.out.println(Main.jmlsp.getByID("Hsetup"));
        System.out.println(Main.jmlsp.getByID("Hreader"));
        System.out.println(Main.jmlsp.getByID("HshowAll"));
        System.out.println(Main.jmlsp.getByID("HshowID"));
        System.out.println(Main.jmlsp.getByID("HcalcAllHours"));
        System.out.println(Main.jmlsp.getByID("HcalcHoursByID"));
    }

    /**
     * Calculating hours per month fo all ids
     *
     * @param month - month to calculate
     * @return
     * @throws IOException
     */
    public String calculateAllHours(String month) throws IOException {
        return calculateHours(month, null);
    }

    /**
     * CalculatingHours for specific ID
     *
     * @param month - month to calculate in form of MMYYYY
     * @param id    - id of employee
     * @return
     * @throws IOException
     */
    public String calculateHours(String month, String id) throws IOException {
        ArrayList<String[]> data = null;
        try {
            if (id == null)
                data = this.dataGetter.getAllData();
            else
                data = this.dataGetter.getDataById(id);
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }

        HashMap<String, Long> hours = new HashMap<>();
        HashMap<String, Long> hoursSaturday = new HashMap<>();
        HashMap<String, Long> hoursSunday = new HashMap<>();
        HashMap<String, Long> hoursPublicHoliday = new HashMap<>();

        for (String[] row : data) {
            try {
                if ((!LocalDateTime.parse(row[4]).format(DateTimeFormatter.ofPattern("MMyyyy")).equals(month))
                        && month != null)
                    continue;
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Not everyone has theirs departes", "ERROR", JOptionPane.ERROR_MESSAGE);
                continue;
            }
            LocalDateTime arrivalTime = LocalDateTime.parse(row[3]);
            LocalDateTime departTime = LocalDateTime.parse(row[4]);

            String idd = row[0];
            if (isPublicHoliday(departTime)) {
                hoursPublicHoliday.put(idd, (hoursPublicHoliday.get(idd) != null ? hoursPublicHoliday
                        .get(idd) : 0)
                        + (departTime.atZone(ZoneId.systemDefault()).toEpochSecond() - arrivalTime.atZone(ZoneId
                        .systemDefault()).toEpochSecond()));
            } else if (departTime.getDayOfWeek() == DayOfWeek.SUNDAY) {
                hoursSunday.put(idd, (hoursSunday.get(idd) != null ? hoursSunday.get(idd) : 0)
                        + (departTime.atZone(ZoneId.systemDefault()).toEpochSecond() - arrivalTime.atZone(ZoneId
                        .systemDefault()).toEpochSecond()));
            } else if (departTime.getDayOfWeek() == DayOfWeek.SATURDAY) {
                hoursSaturday.put(idd, (hoursSaturday.get(idd) != null ? hoursSaturday.get(idd) : 0)
                        + (departTime.atZone(ZoneId.systemDefault()).toEpochSecond() - arrivalTime.atZone(ZoneId
                        .systemDefault()).toEpochSecond()));
            } else {
                hours.put(idd, (hours.get(idd) != null ? hours.get(idd) : 0)
                        + (departTime.atZone(ZoneId.systemDefault()).toEpochSecond() - arrivalTime.atZone(ZoneId
                        .systemDefault()).toEpochSecond()));
            }
        }
        double totalHours = 0;
        StringBuilder output = new StringBuilder();
        System.out.println(Main.jmlsp.getByID("Weekdays"));
        output.append(Main.jmlsp.getByID("Weekdays")).append("\n");
        addingDataFunction(hours, output);

        System.out.println(Main.jmlsp.getByID("Saturdays"));
        output.append(Main.jmlsp.getByID("Saturdays")).append("\n");
        addingDataFunction(hoursSaturday, output);

        System.out.println(Main.jmlsp.getByID("Sundays"));
        output.append("Sundays").append("\n");
        addingDataFunction(hoursSunday, output);

        System.out.println(Main.jmlsp.getByID("PublicHolidays"));
        output.append(Main.jmlsp.getByID("PublicHolidays")).append("\n");
        addingDataFunction(hoursPublicHoliday, output);

        return output.toString();
    }

    private void addingDataFunction(@NonNull HashMap<String, Long> hours, @NonNull StringBuilder output) {
        for (String ids : hours.keySet()) {
            System.out.println("\t" + ids + " : " + Double.valueOf(hours.get(ids)) / 60D / 60D);
            output.append("    ").append(ids).append(" : ").append(Double.valueOf(hours.get(ids)) / 60D / 60D)
                    .append("\n");
        }
    }

    /**
     * Getting data dor public holidays
     *
     * @param date
     * @return True if date is Public Holiday or False if it's
     * @throws IOException
     */
    private boolean isPublicHoliday(@NonNull LocalDateTime date) throws IOException {
        return isPublicHoliday(date, Main.config.getConfigByName("State"));
    }

    private boolean isPublicHoliday(LocalDateTime date, String lang) throws IOException {
        URL url;
        InputStream is;
        BufferedReader br;
        StringBuilder file = new StringBuilder();
        url = new URL("https://date.nager.at/api/v2/publicholidays/" + date.getYear() + "/" + lang);
        is = url.openStream();
        String line;
        br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            file.append(line);
        }
        return file.toString().contains(
                date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
    }

    /**
     * Adding new user to DB
     *
     * @param id
     * @param name
     * @param surname
     * @throws SQLException
     */
    public void addNewUser(String id, String name, String surname) throws SQLException {
        this.mysqlDatabaseConnector.insert("employeeEAN, name, surname", "'" + id + "', '"
                + name + "', '" + surname + "'", "employee");
    }
}
