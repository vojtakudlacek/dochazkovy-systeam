package cz.zahradnictvivedrovice.kudlacek.vojtech.DS.frontend;

import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.CLI.Main;
import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend.DB.MysqlDatabaseConnector;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.HashMap;

public class SettingsWindow extends JFrame {
    /**
     * Class settings window
     * Shows settings
     */
    private final boolean isMain;

    public SettingsWindow(boolean isMain) {
        super(Main.jmlsp.getByID("Setts"));
        this.isMain = isMain;
        ClassLoader classLoader = getClass().getClassLoader();
        try {
            this.setIconImage(ImageIO.read(new File(classLoader.getResource("logo.png").getPath())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (isMain)
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        else
            setDefaultCloseOperation(EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(600, 300));
        initComponents();
        setLayout(new GridLayout(0, 3, 10, 10));
        pack();
        setVisible(true);
    }

    SettingsWindow() {
        this(true);
    }

    /**
     * Adding components to settings
     */
    private void initComponents() {

        for (int i = 0; i < 1; i++) {
            addEmpty();
        }
        HashMap<String, JTextField> settings = new HashMap<>();

        add(new JLabel(Main.jmlsp.getByID("DBSett")));

        for (int i = 0; i < 1; i++) {
            addEmpty();
        }

        JTextField DBUser = new JTextField();
        JTextField DBDB = new JTextField();
        JPasswordField DBPass = new JPasswordField();
        add(new JLabel(Main.jmlsp.getByID("DBAddr")));
        JTextField DBAddress = new JTextField();
        settings.put("DBHost", DBAddress);
        add(DBAddress);
        JButton testButton = new JButton(Main.jmlsp.getByID("DBTest"));
        testButton.addActionListener(actionEvent -> {
            try {
                MysqlDatabaseConnector testDatabase = new MysqlDatabaseConnector(new URI(DBAddress.getText()), DBUser.getText(), String.valueOf(DBPass.getPassword()), DBDB.getText());
                JOptionPane.showMessageDialog(this, Main.jmlsp.getByID("Suc"),Main.jmlsp.getByID("Suc"), JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, Main.jmlsp.getByID("BConfig"), Main.jmlsp.getByID("BConfig"), JOptionPane.ERROR_MESSAGE);
            }
        });
        add(testButton);

        add(new JLabel(Main.jmlsp.getByID("DBUser")));
        settings.put("DBUser", DBUser);
        add(DBUser);
        addEmpty();

        add(new JLabel(Main.jmlsp.getByID("DBPass")));
        add(DBPass);
        settings.put("DBPass", DBPass);
        addEmpty();

        add(new JLabel(Main.jmlsp.getByID("DBName")));
        add(DBDB);
        settings.put("DBDB", DBDB);
        addEmpty();

        add(new JLabel(Main.jmlsp.getByID("SState")));
        String[] states = {"AD", "AL", "AR", "AT", "AU", "AX", "BB", "BE", "BG", "BJ", "BO", "BR", "BS", "BW", "BY", "BZ", "CA", "CH", "CL", "CN", "CO", "CR", "CU", "CY", "CZ", "DE", "DK", "DO", "EC", "EE", "EG", "ES", "FI", "FO", "FR", "GA", "GB", "GD", "GL", "GM", "GR", "GT", "GY", "HN", "HR", "HT", "HU", "ID", "IE", "IM", "IS", "IT", "JE", "JM", "JP", "LI", "LS", "LT", "LU", "LV", "MA", "MC", "MD", "MG", "MK", "MN", "MT", "MX", "MZ", "NA", "NE", "NI", "NL", "NO", "NZ", "PA", "PE", "PL", "PR", "PT", "PY", "RO", "RS", "RU", "SE", "SI", "SJ", "SK", "SM", "SR", "SV", "TN", "TR", "UA", "US", "UY", "VA", "VE", "VN", "ZA", "ZW",};
        JComboBox<String> SState = new JComboBox<>(states);
        SState.setSelectedItem(!Main.config.isNameSet("State") ? "US" : Main.config.getConfigByName("State"));
        add(SState);
        addEmpty();

        add(new JLabel(Main.jmlsp.getByID("SLang")));
        Object[] langs = Main.jmlsp.getLanguages().toArray();
        JComboBox<Object> SLang = new JComboBox<>(langs);
        SLang.setSelectedItem(!Main.config.isNameSet("lang") ? "en_us.lang" : Main.config.getConfigByName("lang"));
        add(SLang);
        addEmpty();

        JButton OK = new JButton(Main.jmlsp.getByID("OK"));
        OK.addActionListener(actionEvent -> {
            try {
                saveSettings(settings, true);
                Main.config.setConfig("State", (String) SState.getSelectedItem());
                Main.config.setConfig("lang", (String) SLang.getSelectedItem());
                Main.jmlsp.setLanguage((String) SLang.getSelectedItem());
            } catch (URISyntaxException | SQLException | ClassNotFoundException e) {
                System.err.println(Main.jmlsp.getByID("DBConnErr"));
            }
        });
        add(OK);
        if (this.isMain) {
            JButton cancel = new JButton(Main.jmlsp.getByID("Cancel"));
            cancel.addActionListener(actionEvent -> this.dispose());
            add(cancel);
        }

        JButton apply = new JButton(Main.jmlsp.getByID("Apply"));
        apply.addActionListener(actionEvent -> {
            try {
                saveSettings(settings, false);
            } catch (URISyntaxException | SQLException | ClassNotFoundException e) {
                System.err.println(Main.jmlsp.getByID("DBConnErr"));
            }
        });
        add(apply);

        setDefaultValues(settings);
    }

    /**
     * Setting default values for textboxes
     *
     * @param settings
     */
    private void setDefaultValues(HashMap<String, JTextField> settings) {
        for (String key : settings.keySet())
            settings.get(key).setText(Main.config.getConfigByName(key));
    }

    /**
     * Saving settings to file
     *
     * @param settings
     * @param close
     * @throws URISyntaxException
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    private void saveSettings(HashMap<String, JTextField> settings, boolean close) throws URISyntaxException,
            SQLException, ClassNotFoundException {
        for (String key : settings.keySet())
            Main.config.setConfig(key, settings.get(key).getText());
        if (isMain)
            Main.cli.mysqlDatabaseConnector = new MysqlDatabaseConnector(new URI(Main.config.getConfigByName("DBHost")),
                    Main.config.getConfigByName("DBUser"), Main.config.getConfigByName("DBPass"), Main.config
                    .getConfigByName("DBDB"));
        if (close)
            dispose();
    }

    /**
     * Adding empty element to grid
     */
    private void addEmpty() {
        add(new Label());
    }

}
