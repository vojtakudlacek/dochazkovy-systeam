package cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend;


import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.*;

/**
 * Java multi language support
 *
 * Loads language files form specified folder and copies main language
 * Service that provides translations by ids
 */
public class JMLSP {

    private final String languagesFolder;
    private final HashMap<String, HashMap<String, String>> languages;
    private String selectedLanguage;

    public JMLSP(String languagesFolder, String selectedLanguage) throws IOException {
        this.languagesFolder = languagesFolder;
        languages = new HashMap<>();
        this.selectedLanguage = selectedLanguage;
        loadFromFiles();
    }

    public Set<String> getLanguages() {
        return languages.keySet();
    }

    public Collection<URL> getResourcesByFolder(String folder){
        try {
            String[] files = new String(JMLSP.class.getClassLoader().getResources(folder).nextElement().openStream()
                    .readAllBytes()).split("\n");
            ArrayList<URL> urls = new ArrayList<>();
            for (String file: files) {
                URL url = JMLSP.class.getClassLoader().getResource(folder + "/" + file);
                urls.add(url);
            }
            return urls;
        } catch (IOException e) {
            System.err.println("Resources can't be loaded");
            return null;
        }
    }

    private void loadFromFiles() throws IOException {
        File file = new File(this.languagesFolder);
        file.deleteOnExit();

        if (!file.isDirectory()) {
            file.mkdirs();
        }

        File basic = new File(this.languagesFolder + "en_us.lang");
        ClassLoader classLoader = getClass().getClassLoader();

        if (!basic.exists())
            try {
                Collection<URL> langs = getResourcesByFolder("langs");

                for (URL lang: langs) {
                    String[] fileName = lang.getFile().split("/");
                    System.out.println(fileName[fileName.length-1]);
                    Files.copy(lang.openStream(),
                            new File(languagesFolder+"/"+fileName[fileName.length-1]).toPath()
                            , StandardCopyOption.REPLACE_EXISTING);

                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        
        File[] files = file.listFiles();
        for (File f : Objects.requireNonNull(files)) {

            BufferedReader br = new BufferedReader(new FileReader(f));
            String line;
            HashMap<String, String> tmp = new HashMap<>();

            while ((line = br.readLine()) != null) {
                tmp.put(line.split("=")[0], line.split("=")[1]);
            }

            languages.put(f.getName(), tmp);
        }

    }

    public void setLanguage(String language) {
        this.selectedLanguage = language;
    }

    public String getByID(String ID) {
        return languages.get(selectedLanguage).get(ID);
    }
}
