package cz.zahradnictvivedrovice.kudlacek.vojtech.DS.frontend;

import com.github.lgooddatepicker.components.DateTimePicker;
import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.CLI.Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;

class DateTimePickerDialog extends JFrame {

    private DateTimePicker dateTimePicker;
    private Button btn;

    /**
     * Dialog for picking date and time
     */
    DateTimePickerDialog() {
        super(Main.jmlsp.getByID("DateTime"));
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setPreferredSize(new Dimension(640, 240));
        initComponents();
        pack();
        setVisible(true);
    }

    private void initComponents() {
        dateTimePicker = new DateTimePicker();
        this.add(dateTimePicker, BorderLayout.NORTH);
        btn = new Button(Main.jmlsp.getByID("OK"));
        this.add(btn, BorderLayout.SOUTH);
    }

    void addActionListener(ActionListener l) {
        btn.addActionListener(l);
    }

    LocalDateTime getDate() {
        return this.dateTimePicker.getDateTimeStrict();
    }


}
