package cz.zahradnictvivedrovice.kudlacek.vojtech.DS.backend;

import java.io.*;
import java.util.HashMap;

public class Configuration {

    private final HashMap<String, String> configData;
    private final File configFile;

    /**
     * Configuration
     *
     * @param configFolder     config folder fo saving config files
     * @param nameOfConfigFile config file
     */
    public Configuration(File configFolder, String nameOfConfigFile) {
        if (!configFolder.exists())
            configFolder.mkdirs();
        configFile = new File(configFolder.getAbsolutePath() + System.getProperty("file.separator")
                + nameOfConfigFile + ".conf");
        configData = new HashMap<>();
        try {
            loadConfig();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * loadConfig function
     *
     * @throws IOException
     */
    private void loadConfig() throws IOException {
        if (!configFile.exists())
            configFile.createNewFile();
        BufferedReader br = new BufferedReader(new FileReader(configFile));
        String line;
        while ((line = br.readLine()) != null) {
            String[] data = line.split("=");
            if (data.length == 1)
                configData.put(data[0], "");
            else
                configData.put(data[0], data[1]);
        }
        br.close();
    }

    /**
     * control function if is name set
     *
     * @param name - name of setting
     * @return set or not set
     */
    public boolean isNameSet(String name) {
        return configData.get(name) != null;
    }

    /**
     * Saving to config file
     *
     * @throws IOException
     */
    private void updateConfig() throws IOException {
        configFile.delete();
        configFile.createNewFile();
        BufferedWriter bw = new BufferedWriter(new FileWriter(configFile));
        for (String key : configData.keySet()) {
            bw.write(key + "=" + configData.get(key));
            bw.newLine();
        }
        bw.flush();
        bw.close();
    }

    /**
     * Getting config for name
     *
     * @param name - name of string
     * @return String - setting
     */
    public String getConfigByName(String name) {
        return configData.get(name);
    }

    /**
     * Setting config value
     *
     * @param name  - name of the config
     * @param value - of the config
     */
    public void setConfig(String name, String value) {
        configData.put(name, value);
        try {
            updateConfig();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
