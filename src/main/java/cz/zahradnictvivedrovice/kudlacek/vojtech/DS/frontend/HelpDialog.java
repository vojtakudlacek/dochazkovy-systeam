package cz.zahradnictvivedrovice.kudlacek.vojtech.DS.frontend;

import cz.zahradnictvivedrovice.kudlacek.vojtech.DS.CLI.Main;
import org.xhtmlrenderer.simple.XHTMLPanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

class HelpDialog extends JFrame {
    /**
     * Help dialog
     */
    HelpDialog() {
        super(Main.jmlsp.getByID("Help"));
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setPreferredSize(new Dimension(640, 480));
        initComponents();
        ClassLoader classLoader = getClass().getClassLoader();
        try {
            this.setIconImage(ImageIO.read(new File(Objects.requireNonNull(classLoader.getResource("logo.png"))
                    .getPath())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        pack();
        setVisible(true);
    }

    /**
     * Initialising components
     */
    private void initComponents() {

        XHTMLPanel xhtmlPanel = new XHTMLPanel();
        try {
            xhtmlPanel.setDocument(getClass().getClassLoader().getResourceAsStream("README.html"),null);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        JScrollPane scrollPane = new JScrollPane(xhtmlPanel);
        add(scrollPane);
    }
}
